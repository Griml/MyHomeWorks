package MyLinkedList;


import java.util.Iterator;

public class ListMain {

    public static void main(String[] args) {


        testAddIncorrectIndex();
        testAddIndexBounds();
        testAddToHead();
        testDeleteFromHead();
        testAddToTail();
        testDeleteFromTail();
        testAddInside();
        testReplace();
    }

    private static void testReplace() {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.addToTail(1);
        myLinkedList.addToTail(2);
        myLinkedList.addToTail(3);
        myLinkedList.addToTail(4);
        myLinkedList.addToTail(5);
        myLinkedList.replace(3,1);
        System.out.println(myLinkedList);
        System.out.println("Done test replace");
    }

    private static void testDeleteFromTail() {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.addToTail(1);
        myLinkedList.addToTail(2);
        myLinkedList.addToTail(3);
        myLinkedList.addToTail(4);
        myLinkedList.addToTail(5);
        myLinkedList.delFromTail(3);
        System.out.println(myLinkedList);
        System.out.println("Done test delete From Tail");
    }

    private static void testDeleteFromHead() {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.addToHead(1);
        myLinkedList.addToHead(2);
        myLinkedList.addToHead(3);
        myLinkedList.addToHead(4);
        myLinkedList.addToHead(5);
        myLinkedList.delFromHead(0);
        System.out.println(myLinkedList);
        System.out.println("Done test Delete From Head");
    }

    private static void testAddToHead() {
        MyLinkedList myLinkedList1 = new MyLinkedList();
        myLinkedList1.addToHead(1);
        myLinkedList1.addToHead(2);
        myLinkedList1.addToHead(3);
        myLinkedList1.addToHead(4);
        myLinkedList1.addToHead(5);
        System.out.println(myLinkedList1);
        System.out.println("Done test Add To Head");
    }


    private static void testAddInside() {
        MyLinkedList myLinkedList = new MyLinkedList();
        System.out.println(myLinkedList);
        myLinkedList.addToTail(1);
        myLinkedList.addToTail(2);
        myLinkedList.addToTail(3);
        myLinkedList.addToTail(4);
        myLinkedList.addToTail(5);
        myLinkedList.addToTail(6);
        myLinkedList.addToTail(7);
        myLinkedList.add(10, 1);
        myLinkedList.add(20, 6);
        System.out.println(myLinkedList);
        System.out.println("Done test Add Inside");
    }

    /**
     * Тестирую add с граничными значениеями индексов
     */
    private static void testAddIndexBounds() {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.add(0, 0);
        myLinkedList.add(2, 1);
        System.out.println("Done test Add Index Bounds");

    }

    private static void testAddIncorrectIndex() {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.add(0, -1);
        myLinkedList.add(0, 1);
        System.out.println("Done test Add Incorrect Index");
    }

    private static void testAddToTail() {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.addToTail(1);
        myLinkedList.addToTail(2);
        myLinkedList.addToTail(3);
        myLinkedList.addToTail(4);
        myLinkedList.addToTail(5);
        System.out.println(myLinkedList);
        System.out.println("Done test Add To Tail");
    }
}


