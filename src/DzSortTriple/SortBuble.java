package DzSortTriple;

import java.util.Arrays;

public class SortBuble {
    public static void main(String[] args) {
        int[] arr = {8, 6, 4, 2};
        enterData(arr);
        sortBub(arr);

    }

    public static void enterData(int[] arr) {
        System.out.println("Пузырьковая сортировка:");
        System.out.println("Исходный массив: " + Arrays.toString(arr));
    }

    public static void printArray(int[] arr, int numb) {
        if (numb > 0) {
            System.out.print(numb + "-й проход ");
        } else {
            System.out.print("           ");
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

    public static void sortBub(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            int tmp;
            for (int j = 0; j < arr.length - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;

                    if (j == 0) {
                        printArray(arr, i + 1);
                    } else {
                        printArray(arr, 0);
                    }
                }

            }
        }
    }
}
