package DzSortTriple;


import java.util.Arrays;

public class SortInsert {
    public static void main(String[] args) {
        int[] arr = {8, 6, 4, 2};
        enterData(arr);
        sortInsert(arr);
    }

    public static void enterData(int[] arr) {

        System.out.println("Сортировка вставкой:");
        System.out.println("Исходный массив: " + Arrays.toString(arr));
    }

    public static void sortInsert(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            for (int j = i; j > 0 && arr[j - 1] > arr[j]; j--) {
                int tmp = arr[j - 1];
                arr[j - 1] = arr[j];
                arr[j] = tmp;
            }
            System.out.println(i + " - проход: " + Arrays.toString(arr));
        }
    }


}




