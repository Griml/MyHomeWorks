package DzSortTriple;


import java.util.Arrays;

public class SortSelect {
    public static void main(String[] args) {
        int[] arr = {8, 6, 4, 2};
        enterData(arr);
        sortSel(arr);
    }

    public static void enterData(int[] arr) {

        System.out.println("Сортировка выбором:");
        System.out.println("Исходный массив: " + Arrays.toString(arr));
    }

    public static void sortSel(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            int max = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[max]) {
                    max = j;
                }
                int tmp = arr[i];
                arr[i] = arr[max];
                arr[max] = tmp;

            }
            System.out.println(i + 1 + "-й проход " + Arrays.toString(arr));
        }
    }
}
