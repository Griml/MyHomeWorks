package HWpassport;


public class Documents {
    private String _name;
    private String _lastName;
    private String _midleName;
    private String _serial;
    private int _nuber;
    private String _monthGive;
    private int _yearOfIssue;
    private int _dayOfIssue;
    private String _country;

    public Documents(String _name, String _lastName, String _midleName, String _serial, int _nuber, String _monthGive, int _yearOfIssue, int _dayOfIssue, String _country) {
        this._name = _name;
        this._lastName = _lastName;
        this._midleName = _midleName;
        this._serial = _serial;
        this._nuber = _nuber;
        this._monthGive = _monthGive;
        this._yearOfIssue = _yearOfIssue;
        this._dayOfIssue = _dayOfIssue;
        this._country = _country;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_lastName() {
        return _lastName;
    }

    public void set_lastName(String _lastName) {
        this._lastName = _lastName;
    }

    public String get_midleName() {
        return _midleName;
    }

    public void set_midleName(String _midleName) {
        this._midleName = _midleName;
    }

    public String get_serial() {
        return _serial;
    }

    public void set_serial(String _serial) {
        this._serial = _serial;
    }

    public int get_nuber() {
        return _nuber;
    }

    public void set_nuber(int _nuber) {
        this._nuber = _nuber;
    }

    public String get_monthGive() {
        return _monthGive;
    }

    public void set_monthGive(String _monthGive) {
        this._monthGive = _monthGive;
    }

    public int get_yearOfIssue() {
        return _yearOfIssue;
    }

    public void set_yearOfIssue(int _yearOfIssue) {
        this._yearOfIssue = _yearOfIssue;
    }

    public int get_dayOfIssue() {
        return _dayOfIssue;
    }

    public void set_dayOfIssue(int _dayOfIssue) {
        this._dayOfIssue = _dayOfIssue;
    }

    public String get_country() {
        return _country;
    }

    public void set_country(String _country) {
        this._country = _country;
    }

    @Override
    public String toString() {
        return
                "name  " + _name + '\n' +
                "lastName  " + _lastName + '\n' +
                "midleName  " + _midleName + '\n' +
                "serial  " + _serial + '\n' +
                "nuber  " + _nuber +'\n' +
                "monthGive  " + _monthGive + '\n' +
                "yearOfIssue  " + _yearOfIssue +'\n' +
                "dayOfIssue  " + _dayOfIssue +'\n' +
                "country  " + _country +'\n';
    }
}
