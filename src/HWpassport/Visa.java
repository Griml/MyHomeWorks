package HWpassport;

public class Visa {
    private String _typeOfVisaSerial;
    private String _dateStart;
    private String _dateFinish;
    private String _inCountry;

    public Visa(String _typeOfVisaSerial, String _dateStart, String _dateFinish, String _inCountry) {
        this._typeOfVisaSerial = _typeOfVisaSerial;
        this._dateStart = _dateStart;
        this._dateFinish = _dateFinish;
        this._inCountry = _inCountry;

    }

    public String get_typeOfVisaSerial() {
        return _typeOfVisaSerial;
    }

    public void set_typeOfVisaSerial(String _typeOfVisaSerial) {
        this._typeOfVisaSerial = _typeOfVisaSerial;
    }

    public String get_dateStart() {
        return _dateStart;
    }

    public void set_dateStart(String _dateStart) {
        this._dateStart = _dateStart;
    }

    public String get_dateFinish() {
        return _dateFinish;
    }

    public void set_dateFinish(String _dateFinish) {
        this._dateFinish = _dateFinish;
    }

    public String get_inCountry() {
        return _inCountry;
    }

    public void set_inCountry(String _inCountry) {
        this._inCountry = _inCountry;
    }

    @Override
    public String toString() {
        return "Visa: " +
                "typeOfVisaSerial - " + _typeOfVisaSerial +", "+
                "dateStart  " + _dateStart +", "+
                "dateFinish  " + _dateFinish +", "+
                "Country:  " + _inCountry + '\n' ;
    }
}
