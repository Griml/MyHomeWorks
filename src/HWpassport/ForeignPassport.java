package HWpassport;


public class ForeignPassport extends Documents {

    public static final int STEP = 10;
    private int lastVisaIndex = 0;
    private Visa[] allVisa;


    public ForeignPassport(String _name, String _lastName, String _midleName, String _serial, int _nuber, String _monthGive, int _yearOfIssue, int _dayOfIssue, String _country) {
        super(_name, _lastName, _midleName, _serial, _nuber, _monthGive, _yearOfIssue, _dayOfIssue, _country);
        allVisa = new Visa[STEP];
    }

    public void addVisa(Visa visa) {
        if (lastVisaIndex >= allVisa.length) {
            Visa[] tmp = new Visa[allVisa.length + STEP];
            System.arraycopy(allVisa, 0, tmp, 0, allVisa.length);
            allVisa = tmp;
        }
        allVisa[lastVisaIndex] = visa;
        lastVisaIndex++;
    }

    @Override

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ForeignPassport:\n");
        builder.append(super.toString());
        for(Visa visa: allVisa) {
            if(visa != null) {
                builder.append(visa);
            }
        }
        return builder.toString();
    }

}
