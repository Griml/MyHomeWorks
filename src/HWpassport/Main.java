package HWpassport;

import com.sun.org.apache.xpath.internal.SourceTree;

/*
Создайте класс Passport,
 который будет содержать паспортные данные гражданина Украины. С помощью наследования (расширения),
 создайте класс ForeignPassport (паспорт для выезда за границу) производный от Passport.
 Загранпаспорт содержит данные о визах и номер загранпаспорта.
 Визу рекомендую реализовать как отдельный класс и типом визы и датой открытия и закрытия
 (плюс дополнительные свойства на ваше усмотрение)
 */
public class Main {
    public static void main(String[] args) {

        Passport passport = new Passport("Maxim", "Zhilyn", "Sergeevish", "AH", 453544, "february", 2005, 6, "Ukraine");
      Visa[] _visa = {
                new Visa("A1", "01.02.2017",
                        "01.05.2017", "USA"),
                new Visa("A1", "01.02.2017",
                        "01.05.2017", "Poland"),
                new Visa("A1", "01.05.2017",
                        "01.05.2017", "England")};
        ForeignPassport foreignPassport = new ForeignPassport("Maxim", "Zhilyn", "Sergeevish", "AP", 777777, "february", 2016, 1, "Ukraine");
        for (int i = 0; i <_visa.length ; i++)
        {
            foreignPassport.addVisa(_visa[i]);
        }

        System.out.println(passport);
        System.out.println("--------------------------------");
        System.out.println(foreignPassport);
    }
}

