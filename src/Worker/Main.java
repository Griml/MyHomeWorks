package Worker;
/*Построить три класса (базовый и 2 потомка),
  описывающих некоторых работников с почасовой оплатой (один из потомков) и фиксированной оплатой (второй потомок).
  Описать в базовом классе абстрактный метод для расчета среднемесячной заработной платы.
  Для «повременщиков» формула для расчета такова: «среднемесячная заработная плата = 20.8 * 8 * почасовую ставку»,
  для работников с фиксированной оплатой «среднемесячная заработная плата = фиксированной месячной оплате».

a) Упорядочить всю последовательность работников по убыванию среднемесячного заработка.
   При совпадении зарплаты – упорядочивать данные по алфавиту по имени.
   Вывести идентификатор работника, имя и среднемесячный заработок для всех элементов списка.

b) Вывести первые 5 имен работников из полученного в пункте а) списка.

c) Вывести последние 3 идентификатора работников из полученного в пункте а) списка.*/


public class Main {

    public static void main(String[] args) {


        Workers mechanic1 = new HourWorker("Vasya", "Alexov", 12);
        Workers mechanic2 = new HourWorker("Leonid", "Safonov", 12);
        Workers mechanic3 = new HourWorker("Oleg", "Mironen", 12);
        Workers packer2 = new MonthWorker("Alina", "Malini", 500);
        Workers packer1 = new MonthWorker("Abdula", "Abramov", 500);
        Workers packer3 = new MonthWorker("Maximus", "Maximov", 1000);
        Workers[] _workers = {mechanic1, mechanic2, mechanic3, packer1, packer2, packer3};

        int n = _workers.length;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (_workers[j - 1]._countSalary()
                        .compareTo(_workers[j]._countSalary()) < 0) {
                    Workers temp = _workers[j - 1];
                    _workers[j - 1] = _workers[j];
                    _workers[j] = temp;
                }
            }
        }
        System.out.println("----------------------------------------------");
        System.out.println("Список а): ");
        for (int i = 0; i < _workers.length; ++i) {
            System.out.printf("%s\t\t%s\t\t%s\t\t%f%n",
                    _workers[i].getId(),
                    _workers[i].getName(),
                    _workers[i].getLastName(),
                    _workers[i]._countSalary());
        }


        System.out.println("--------------------------------------------------");
        System.out.println("Первых пять имен списка а):");
        for (int i = 0; i < _workers.length; ++i) {
            if (i < 5) {
                System.out.println(_workers[i].getName());
            }
        }
        System.out.println("--------------------------------------------------");
        System.out.println("Последние три идентификатора списка а):");
        for (int i = 0; i < _workers.length; ++i) {
            if (_workers.length - 1 - i < 3) {
                System.out.println(_workers[i].getId());
            }
        }


    }


}
