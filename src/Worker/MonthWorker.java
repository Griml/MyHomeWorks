package Worker;


import java.math.BigDecimal;

public class MonthWorker extends Workers {
    public MonthWorker(String name, String lastName, int rate) {
        super(name, lastName, rate);
    }

    @Override
    public BigDecimal _countSalary() {
        return getRate();
    }

}
