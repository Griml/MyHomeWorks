package Worker;


import java.math.BigDecimal;
import java.math.MathContext;

public class HourWorker extends Workers {
    public HourWorker(String name, String lastName, int rate) {
        super(name, lastName, rate);
    }

    @Override
    public BigDecimal _countSalary() {
        BigDecimal rate = getRate();
        BigDecimal hoursInDay = new BigDecimal(8, MathContext.DECIMAL64);
        BigDecimal workingDaysInMonth = new BigDecimal(20.8, MathContext.DECIMAL64);
        return hoursInDay.multiply(workingDaysInMonth)
                .multiply(rate);
    }


}
