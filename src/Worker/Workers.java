package Worker;


import java.math.BigDecimal;
import java.math.MathContext;

public abstract class Workers {

    private static int counter;

    private String _name;
    private String _lastName;
    private BigDecimal _rate;
    private int id;


    public Workers(String name, String lastName, int rate) {
        _name = name;
        _lastName = lastName;
        _rate = new BigDecimal(rate, MathContext.DECIMAL64);
        counter++;
        id = counter;
    }

    public static int getCounter() {
        return counter;
    }


    public String getName() {
        return _name;
    }

    public void setName(String _name) {
        this._name = _name;
    }

    public String getLastName() {
        return _lastName;
    }

    public void setLastName(String _lastName) {
        this._lastName = _lastName;
    }

    public BigDecimal getRate() {
        return _rate;
    }

    public void setRate(BigDecimal _rate) {
        this._rate = _rate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public abstract BigDecimal _countSalary();

    @Override
    public String toString() {
        return "id=" + id +
                ", name='" + _name +
                ", name='" + _lastName +
                ", rate=" + _rate +
                ", salary = " + this._countSalary();
    }

}

