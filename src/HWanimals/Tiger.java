package HWanimals;


public class Tiger extends Animal {

    public Tiger() {
        super();
    }

    @Override
    public void doSound() {
        toString("Rrrr");
    }

    private void toString(String rrrr) {
        System.out.println("Tiger do sound: Rrrrr");
    }
}
