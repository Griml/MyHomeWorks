package HWanimals;
/*Реализовать абстрактный класс Animal с абстрактным методом doSound() - звук конкретного животного.
 Отнаследовать от него 3 класса конкретных животных и реализовать абстрактный метод.
 Создать массив различных животных и в цикле вызвать у каждого doSound()*/



public class Main {
    public static void main(String[] args) {
        Animal[] animal;
        animal = new Animal[3];
        animal[0] = new Lion();
        animal[1] = new Tiger();
        animal[2] = new Panter();
        for (int i = 0; i <animal.length ; i++) {
            animal[i].doSound();
        }

    }
}
