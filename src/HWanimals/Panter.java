package HWanimals;


public class Panter extends Animal {

    public Panter() {
        super();
    }

    @Override
    public void doSound() {
        toString("Augrr");
    }

    private void toString(String augrr) {
        System.out.println("Panter do sound: Augrr");
    }
}
