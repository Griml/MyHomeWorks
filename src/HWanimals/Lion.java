package HWanimals;


public class Lion extends Animal {

    public Lion() {
        super();
    }

    @Override
    public void doSound() {
        toString("Agrrr");
    }

    private void toString(String agrrr) {
        System.out.println("Lion do sound: Agrrr");
    }
}
